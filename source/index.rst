Welcome to Narupa's documentation!
==================================

* `Download the VR client <https://gitlab.com/intangiblerealities/narupa-applications/narupa-imd/-/jobs/artifacts/master/download?job=build-StandaloneWindows64>`_
* :ref:`Install the software <Installation>`
* `Run a session in the cloud <httpa://app.narupa.xyz>`_
* :ref:`Read tutorials <Tutorials>`

Interact with molecules in virtual reality
------------------------------------------

Narupa lets you enter virtual reality and steer molecular dynamics simulations
in real time. Explore rare events and try docking poses.

* `Download the VR client <https://gitlab.com/intangiblerealities/narupa-applications/narupa-imd/-/jobs/artifacts/master/download?job=build-StandaloneWindows64>`_
* `Get the source code of the VR client <https://gitlab.com/intangiblerealities/narupa-applications/narupa-imd>`_


Use insightful visuals
----------------------

Use python to control molecular representations.

* `Read the documentation on representations <https://gitlab.com/intangiblerealities/narupa-protocol/-/blob/master/examples/fundamentals/visualisations.ipynb>`_

Explore with others
-------------------

Collaborate in a shared virtual space while sharing or not a physical space. Host a session in the cloud to share the experience with others across the world.

* `Host a session in the cloud <https://app.narupa.xyz>`_


Customise your workflow
-----------------------

Narupa uses a customisable python server. Using the API you can integrate different physics engine and integrate Narupa into your existing workflow.

Narupa is free, open source and distributed under the
`GNU GPLv3 <https://gitlab.com/intangiblerealities/narupa-protocol/blob/master/License>`_ license.
You can look at and contribute to the code for building server applications `here <https://gitlab.com/intangiblerealities/narupa-protocol/>`_,
and our VR applications such as `iMD-VR <https://gitlab.com/intangiblerealities/narupa-applications/narupa-imd/>`_ and 
`Narupa Builder <https://gitlab.com/intangiblerealities/narupa-applications/narupa-builder/>`_.

* :ref:`Read tutorials <Tutorials>`
* `Get the source code for the server <https://gitlab.com/intangiblerealities/narupa-protocol>`_

Citation
###################

If you find Narupa useful, please cite the following
`paper <https://doi.org/10.1063/1.5092590>`_:

Jamieson-Binnie, A. D., O’Connor, M. B., Barnoud, J., Wonnacott, M. D.,
Bennie, S. J., & Glowacki, D. R. (2020, August 17). Narupa iMD: A VR-Enabled
Multiplayer Framework for Streaming Interactive Molecular Simulations. ACM
SIGGRAPH 2020 Immersive Pavilion. SIGGRAPH ’20: Special Interest Group on
Computer Graphics and Interactive Techniques Conference.
https://doi.org/10.1145/3388536.3407891

Bib file:

.. literalinclude:: narupa.bib

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation.rst
   tutorials/tutorials.rst
   concepts/concepts.rst
   python/modules.rst




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
