.. _tutorials:

Tutorials
==================================

This section provides tutorials on getting started with Narupa.
We assume familiarity with setting up simulations in whichever 
framework you're running with Narupa.

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   ase.rst

Further reading
---------------

For more details, look at the documentation for the individual :ref:`modules <narupa-protocol>`.