Concepts
==================================

This section provides an overview of the main concepts in Narupa.

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   architecture.rst
   frames.rst